package fr.ldnr.formation.voyage4boot;

import org.springframework.stereotype.Service;

/**
 * @author mic
 *
 */
@Service // @Component
public class IndeService {
	public double getPrix(int voyageurs) {
		return 345.90 + 563.80 * voyageurs;
	}
}
