
package fr.ldnr.formation.voyage4boot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author mic
 *
 */
@Entity
public class Demande {
	private int id;
	private String nom;
	private String tel;
	
	public Demande() {
		this(null, null);
	}
	public Demande(String n, String t) {
		id = 0;
		nom = n;
		tel = t;
	}
	
	@Override
	public String toString() {
		return id+" : "+nom+" ("+tel+")";
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(nullable = false, length = 100)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(nullable = false, length = 25)
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}

	
}
