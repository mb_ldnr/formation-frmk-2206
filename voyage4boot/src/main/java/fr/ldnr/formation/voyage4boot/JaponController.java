
package fr.ldnr.formation.voyage4boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author mic
 *
 */
@Controller
public class JaponController {

	private JaponService japonService;
	
	//@RequestMapping(path="/japon",method=RequestMethod.GET)
	@GetMapping("/japon")
	public String afficher() {
		return "japon-form";
	}
	
	@PostMapping("/japon")
	public String recevoir(Model model,
			@RequestParam("nom") String nom, 
			@RequestParam("tel") String telephone) {
		// si le nom ou/et le telephone sont incorrect : revenir au formulaire
		if(nom.length()<5 || telephone.length()<10 || 
				!telephone.matches("^[0-9]+$")) {
			model.addAttribute("message", "Erreurs dans le formulaire");
			return "japon-form";
		} else {
			japonService.enregistrer(nom, telephone);
			return "japon-ok";
		}
	}
	
	// http://localhost:8081/japon/liste
	@RequestMapping("/japon/liste")
	public String lister(Model model) {
		model.addAttribute("demandes", japonService.lireDemandes());
		return "japon-liste";
	}

	@Autowired
	public void setJaponService(JaponService japonService) {
		this.japonService = japonService;
	}	
}
