package fr.ldnr.formation.voyage4boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mic
 *
 */
@Controller
public class IndeController {
	
	private IndeService indeService;
	
	// accès : http://localhost:8081/inde
	@RequestMapping("/inde")
	public String afficher() {
		return "inde";
	}
	
	// accès : http://localhost:8081/inde/tarifs
	@RequestMapping("/inde/tarifs")
	public String afficherTarifs(Model model) {
		double pour1 = indeService.getPrix(1);
		double pour3 = indeService.getPrix(3);
		model.addAttribute("pour1", pour1);
		model.addAttribute("pour3", pour3);
		return "inde-tarifs";
	}

	@Autowired
	public void setIndeService(IndeService indeService) {
		this.indeService = indeService;
	}	
}
