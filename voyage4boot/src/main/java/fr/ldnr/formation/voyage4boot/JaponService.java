package fr.ldnr.formation.voyage4boot;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author mic
 *
 */
@Service
public class JaponService {
	private SessionFactory sessionFactory;

	public void enregistrer(String nom, String tel) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(new Demande(nom, tel));
		tx.commit();
		session.close();
	}
	
	public List<Demande> lireDemandes() {
		Session session = sessionFactory.openSession();
		List<Demande> liste = session.
				createQuery("from Demande", Demande.class).list();
		session.close();
		return liste;
	}
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}	
}
