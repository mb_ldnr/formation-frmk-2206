<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Voyages :: Japon</title>
</head>
<body>
	<h1>Voyages au Japon</h1>
	<ul>
	<c:forEach items="${demandes}" var="d">
		<li>${d.nom} : ${d.tel}</li>		
	</c:forEach>
	</ul>
</body>
</html>