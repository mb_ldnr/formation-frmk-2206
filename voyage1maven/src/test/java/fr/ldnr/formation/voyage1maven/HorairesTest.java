package fr.ldnr.formation.voyage1maven;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author mic
 *
 */
public class HorairesTest {

	@Test
	public void testGetOuverture() {
		Horaires h = new Horaires();
		// mercredi
		int obtenu = h.getOuverture(3);
		assertEquals(10, obtenu);
		// dimanche
		obtenu = h.getOuverture(7);
		assertEquals("Pas d'ouverture le dimanche", 
				-1, obtenu);
	}
	
	@Test
	public void testGetFermeture() {
		Horaires h = new Horaires();
		// mardi
		int obtenu = h.getFermeture(2);
		assertEquals(19, obtenu);
		// dimanche
		obtenu = h.getFermeture(7);
		assertEquals("Pas d'ouverture le dimanche", 
				-1, obtenu);
	}
}
