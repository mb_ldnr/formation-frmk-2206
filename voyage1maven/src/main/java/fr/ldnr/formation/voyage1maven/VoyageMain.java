package fr.ldnr.formation.voyage1maven;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author mic
 */
public class VoyageMain {

	public static void main(String[] args) {
		System.out.println("Voyage : app 1");
		try {
			// ouverture du fichier
			InputStream is = VoyageMain.class.getClassLoader().
					getResourceAsStream("presentation.txt");
			// transformation du flux binaire en flux texte lignes
			BufferedReader br = new BufferedReader(
					new InputStreamReader(is));
			String ligne = "";
			while( (ligne = br.readLine()) != null)
				System.out.println(ligne);
			is.close();
		} catch (Exception e) {
			System.err.println("Erreur lecture ressource");
		}
		Horaires h = new Horaires();
		System.out.println("Ouverture le lundi :"+
				h.getOuverture(1) +"h");
	}

}
