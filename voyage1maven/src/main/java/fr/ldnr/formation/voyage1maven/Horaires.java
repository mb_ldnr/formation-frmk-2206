package fr.ldnr.formation.voyage1maven;

/**
 * @author mic
 *
 */
public class Horaires {

	/**
	 * Ouverte du lundi au samedi de 10h à 19h
	 * 
	 * @param jour jour de la semaine
	 * lundi = 1, dimanche = 7
	 * @return heure d'ouverture, ou -1
	 */
	public int getOuverture(int jour) {
		if(jour == 7)
			return -1;
		else
			return 10;
	}

	/**
	 * cf. getOuverture()
	 * @param jour jour de la semaine
	 * lundi = 1, dimanche = 7
	 * @return heure de fermeture, ou -1
	 */
	public int getFermeture(int jour) {
		if(jour == 7)
			return -1;
		else
			return 19;
	}
}
