
package fr.ldnr.formation.voyage3spring;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

/**
 * @author mic
 *
 */
@RedisHash("Destination")
public class Destination {
	private int id;
	@Indexed
	private String nom;
	@Indexed
	private String region;
	
	public Destination() {
		this(0, null, null);
	}
	public Destination(int id, String nom, String region) {
		this.id = id;
		this.nom = nom;
		this.region = region;
	}
	
	@Override
	public String toString() {
		return id+" : "+nom+" ("+region+")";
	}
	
	@Id
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
}
