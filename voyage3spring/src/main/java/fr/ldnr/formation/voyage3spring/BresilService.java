
package fr.ldnr.formation.voyage3spring;

/**
 * @author mic
 *
 */
public class BresilService {

	@Override
	public String toString() {
		return "Pas de voyage au Brésil";
	}
	
	public void init() {
		System.out.println("- Bresil : init()");
	}

	public void destroy() {
		System.out.println("- Bresil : destroy()");
	}

}
