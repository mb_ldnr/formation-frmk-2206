package fr.ldnr.formation.voyage3spring;

import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

/**
 * @author mic
 *
 */
@EnableRedisRepositories("fr.ldnr.formation.voyage3spring")
public class EgypteService {
	private DestinationRepository destinationRepository;

	public DestinationRepository getDestinationRepository() {
		return destinationRepository;
	}

	public void setDestinationRepository(DestinationRepository destinationRepository) {
		this.destinationRepository = destinationRepository;
	}
	
	public void afficher() {
		System.out.println("* Egypte :");
		Destination d1 = new Destination(1, "Karnak", "Sud");
		Destination d2 = new Destination(2, "Port Said", "Nord");
		Destination d3 = new Destination(3, "Alexandrie", "Nord");
		destinationRepository.deleteAll();
		destinationRepository.save(d1);
		destinationRepository.save(d2);
		destinationRepository.save(d3);
		
		for(Destination d:destinationRepository.findAllByOrderByRegion()) {
			System.out.println(" - "+d);
		}
		System.out.println("Sud : ");
		for(Destination d:destinationRepository.findByRegion("Sud")) {
			System.out.println(" - "+d);
		}
		System.out.println("Alexandrie au nord : ");
		for(Destination d:destinationRepository.findByRegionAndNomOrderById(
					"Nord", "Alexandrie")) {
			System.out.println(" - "+d);
		}
	}
	
	
}
