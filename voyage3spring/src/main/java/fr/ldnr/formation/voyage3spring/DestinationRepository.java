package fr.ldnr.formation.voyage3spring;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author mic
 *
 */
@Repository
public interface DestinationRepository
		extends CrudRepository<Destination, Integer> {
	List<Destination> findAllByOrderByRegion();
	List<Destination> findByRegion(String region);
	List<Destination> findByRegionAndNomOrderById(String region, String nom);
}
