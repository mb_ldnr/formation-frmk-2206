package fr.ldnr.formation.voyage3spring;

import java.time.LocalDateTime;

/**
 * aspect de log
 * @author mic
 *
 */
public class LogAspect {
	/**
	 * Greffon
	 */
	public void ecrire() {
		System.out.println("## "+LocalDateTime.now()+" Log");
	}
}
