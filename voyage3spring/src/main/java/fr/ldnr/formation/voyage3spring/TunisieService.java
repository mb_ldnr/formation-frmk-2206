package fr.ldnr.formation.voyage3spring;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * @author mic
 *
 */
public class TunisieService {
	private JdbcTemplate sqlTemplate;
	
	public TunisieService(JdbcTemplate sqlTemplate) {
		this.sqlTemplate = sqlTemplate;
	}
	
	public void remplirDestinations() {
		System.out.println("Remplissage");
		sqlTemplate.execute("CREATE TABLE IF NOT EXISTS "+
				"destinations(nom TEXT, region TEXT)");
		sqlTemplate.execute("DELETE FROM destinations");
		String insertSQL = "INSERT INTO destinations(nom, region)VALUES(?,?)";
		sqlTemplate.update(insertSQL, "Ajim", "Djerba");
		sqlTemplate.update(insertSQL, "Mellita", "Djerba");
		sqlTemplate.update(insertSQL, "Tataouine", "Tataouine");		
	}
	
	public void lireDestinations() {
		// toutes les destinations, ordonnée par région puis nom
		String sqlSelect = "SELECT * FROM destinations ORDER BY region, nom";
		SqlRowSet set = sqlTemplate.queryForRowSet(sqlSelect);
		while(set.next()) {
			System.out.println(" - "+set.getString(1)+" "+set.getString(2));
		}
	}
}
