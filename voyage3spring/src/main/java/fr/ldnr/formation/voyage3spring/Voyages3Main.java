package fr.ldnr.formation.voyage3spring;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDate;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

/**
 * @author mic
 *
 */
public class Voyages3Main {

	public static void main(String[] args) {
		System.out.println("Bienvenue dans Voyages 3 Spring");
		AbstractApplicationContext ctx = new 
				FileSystemXmlApplicationContext("classpath:spring.xml");
		ctx.registerShutdownHook(); // ferme automatiquement à la fin
		
		//faireAmerique(ctx);
		faireAfrique(ctx);
	}
	
	private static void faireAmerique(AbstractApplicationContext ctx) {
		System.out.println(" ** Amérique :");
		try {
			Resource alaskaRes = ctx.getResource("classpath:alaska.txt");
			InputStream alaskaIs = alaskaRes.getInputStream();
			/// afficher le fichier :
			BufferedReader br = new BufferedReader(
					new InputStreamReader(alaskaIs, Charset.forName("UTF-8")));
			String ligne = "";
			while( (ligne = br.readLine()) != null)
				System.out.println(ligne);
			br.close();
		} catch(Exception ex) {
			System.err.println("Erreur : "+ex);
		}

		CanadaService canadaService = 
				ctx.getBean("canadaService", CanadaService.class);
		System.out.println("Canada 14j : "+canadaService.getPrix(14)+"€");
		
		UsaService usaService = ctx.getBean("usaService", UsaService.class);
		System.out.println("USA 17j : "+usaService.getPrix(17)+"€");
		
		MexiqueService mexiqueService = ctx.getBean("mexiqueService", MexiqueService.class);
		System.out.println(mexiqueService);
		
		Integer placesDisponibles = ctx.getBean("dispos", Integer.class);
		System.out.println(placesDisponibles+" places disponibles pour Mexico");
		
		LocalDate aujourdhui = ctx.getBean("aujourdhui", LocalDate.class);
		System.out.println("Aujourd'hui : "+aujourdhui);

		LocalDate prochainDepart = ctx.getBean("prochainDepart", LocalDate.class);
		System.out.println("Prochain départ : "+prochainDepart);
	
		BresilService bresilService = ctx.getBean("bresilService", BresilService.class);
		System.out.println(bresilService);
		
		MexiqueService yucatanService = ctx.getBean("mexiqueService", MexiqueService.class);
		yucatanService.setPrix(1299);
		System.out.println(yucatanService); // 1299€
		System.out.println(mexiqueService);	// singleton : 1299€
		
	}
	private static void faireAfrique(AbstractApplicationContext ctx) {
		TunisieService tunisieService = 
				ctx.getBean("tunisieService", TunisieService.class);
		tunisieService.remplirDestinations();
		tunisieService.lireDestinations();
		
		EgypteService egypteService = 
				ctx.getBean("egypteService", EgypteService.class);
		egypteService.afficher();
	}
}
