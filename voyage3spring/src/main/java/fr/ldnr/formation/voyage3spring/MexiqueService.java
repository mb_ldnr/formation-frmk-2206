
package fr.ldnr.formation.voyage3spring;

import java.time.LocalDate;

/**
 * @author mic
 *
 */
public class MexiqueService {
	private double prix;
	private LocalDate depart;
	
	public MexiqueService() {
		this(0,null);
	}
	
	public MexiqueService(double p, LocalDate d) {
		this.prix = p;
		this.depart = d;
	}
	
	@Override
	public String toString() {
		return "Mexique : "+prix+"€ (depart le "+depart+")";
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public LocalDate getDepart() {
		return depart;
	}

	public void setDepart(LocalDate depart) {
		this.depart = depart;
	}
	
}
