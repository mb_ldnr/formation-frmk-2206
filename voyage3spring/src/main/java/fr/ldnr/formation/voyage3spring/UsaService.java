/**
 * 
 */
package fr.ldnr.formation.voyage3spring;

/**
 * @author mic
 *
 */
public class UsaService {
	private int prixJournee;
	private int[] prixAvion;
	private int promo; // 0..100
	
	public int getPrix(int jours) {
		return (jours*prixJournee+prixAvion[0]+prixAvion[1])*(100-promo)/100;
	}
	
	public int getPrixJournee() {
		return prixJournee;
	}
	public void setPrixJournee(int prixJournee) {
		this.prixJournee = prixJournee;
	}
	public int[] getPrixAvion() {
		return prixAvion;
	}
	public void setPrixAvion(int[] prixAvion) {
		this.prixAvion = prixAvion;
	}
	public int getPromo() {
		return promo;
	}
	public void setPromo(int promo) {
		this.promo = promo;
	}
	
	
	
}
