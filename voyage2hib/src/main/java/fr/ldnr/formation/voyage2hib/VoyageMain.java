package fr.ldnr.formation.voyage2hib;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author mic
 *
 */
public class VoyageMain {

	public static void main(String[] args) {
		System.out.println("Voyage 2 : bienvenue");
		
		// Données du fichier .cfg.xml
		Configuration config = new Configuration();
		// usine à Session configurée
		SessionFactory sessionFact = config.configure().buildSessionFactory();
		// session
		Session session = sessionFact.openSession();
		
		insertions(session);
		modifications(session);
		lectureParCriteres(session);
		lectureParHQL(session);
		lectureParSQL(session);
		insertionsAvecRelations(session);
		lectureAvecRelations(session);
		insertionsPlusieursAPlusieurs(session);
		lecturePlusieursAPlusieurs(session);
		
		session.close();
	}
	
	private static void insertions(Session session) {
		Transaction tx = session.beginTransaction();
		try {
			Destination d1 = new Destination("Cordoue", "Espagne", 4);			
			System.out.println("d1 : "+d1);
			d1.setJours(d1.getJours()+1);
			System.out.println("* dirty (1) : "+session.isDirty());
			session.save(d1);
			System.out.println("d1 enregistré : "+d1);
			session.save(d1); // rien, d1 est deja dans le cache d'Hibernate
			Destination d1bis = d1;
			session.save(d1bis); // rien non plus
			System.out.println("* dirty (2) : "+session.isDirty());
			
			d1.setJours(d1.getJours()+1);
			d1.setNom("Cordoue - Alhambra");
			// Interdit : d1.setId(10);
			
			System.out.println("* dirty (3) : "+session.isDirty()); // true!
			session.flush();
			System.out.println("Flush réalisé");
			System.out.println("* dirty (4) : "+session.isDirty());
			System.out.println("* session : "+session.getStatistics()); //1
			
			Destination d2 = new Destination("Camp Nou", "Catalogne", 1);
			session.save(d2);
			System.out.println("* session : "+session.getStatistics()); //2
			session.evict(d2);
			// session.clear();
			System.out.println("* session : "+session.getStatistics()); //1
			d2.setJours(d2.getJours()+1);
			
			Destination d3 = new Destination("Bilbao", "Espagne", 2);
			session.save(d3);
			session.flush();
			session.delete(d3);
			System.out.println("* session : "+session.getStatistics()); //2!!
			System.out.println("D3 : " + d3);
			
			// ajouter 4 destinations en italie et au portugal
			session.save(new Destination("Venise", "Italie", 3));
			session.save(new Destination("Florence", "Italie", 4));
			session.save(new Destination("Sintra", "Portugal", 2));
			session.save(new Destination("Faro", "Portugal", 4));			
			System.out.println("* session : "+session.getStatistics()); //6
			
			tx.commit();
		} catch(Exception ex) {
			System.err.println("Erreur : "+ex);
			tx.rollback();
		}
	}

	private static void modifications(Session session) {
		System.out.println("*** Modifications");
		session.clear();
		Transaction tx = session.beginTransaction();
		Destination d1 = session.load(Destination.class, 1);
		System.out.println("D1 (loaded) : "+d1);

		Destination d2 = session.get(Destination.class, 2);
		System.out.println("D2 (got) : "+d2);
		
		Destination d89 = session.get(Destination.class, 89);
		System.out.println("D89 (got) : "+d89); // null
		
		// Enlever 1j à la seconde destination :
		d2.setJours(d2.getJours()-1);
		session.evict(d2);
		// session.update(d2); // java->DB,cache
		// session.saveOrUpdate(d2); // java->DB,cache, en fonction de getId()==0
		session.refresh(d2); // DB->java,cache en fonction de getId()
		System.out.println("D2 : "+d2);
	
		tx.commit();
	}

	private static void lectureParCriteres(Session session) {
		System.out.println("*** Lecture par critère");
		session.clear();
		// objectif SQL : select * from destinations where id>1 order by nom
		// constructeur de requêtes
		CriteriaBuilder cb = session.getCriteriaBuilder();
		// requete
		CriteriaQuery<Destination> toutesSaufPremiere = 
				cb.createQuery(Destination.class);
		// table racine de la requete (from...)
		Root<Destination> table = toutesSaufPremiere.from(Destination.class);
		// select *
		toutesSaufPremiere.select(table);
		// order by
		toutesSaufPremiere.orderBy(cb.asc(table.get("nom")));
		// where (greater than)
		toutesSaufPremiere.where(cb.gt(table.<Integer>get("id"), 1));
		
		List<Destination> listeToutesSaufPremiere = 
				session.createQuery(toutesSaufPremiere).list();
		for(Destination d:listeToutesSaufPremiere)
			System.out.println(" - "+d);
		
	}

	private static void lectureParHQL(Session session) {
		System.out.println("*** Lecture par HQL");
		session.clear();
		String toutesSaufPremiereHQL = 
				"from Destination where id>1 order by nom";
		List<Destination> listeToutesSaufPremiere = session.
				createQuery(toutesSaufPremiereHQL, Destination.class).list();
		for(Destination d:listeToutesSaufPremiere)
			System.out.println(" - "+d);
		
		System.out.println("* Destinations courtes :");
		int maxJours = 3;
		// non : String courtesHQL = "from Destination where jours < "+maxJours;		
		String courtesHQL = "from Destination where jours < :mj";
		List<Destination> listeCourtes = session.
				createQuery(courtesHQL, Destination.class).
				setParameter("mj", maxJours).
				list();
		for(Destination d:listeCourtes)
			System.out.println(" - "+d);
		
		System.out.println("* Destinations de durée moyenne dont "+
				"le pays ne commence pas par \"Po\" :");
		int minJours = 3;
		maxJours = 5;
		String eviter = "Po%";
		String moyennesHQL = "from Destination where "+
				"jours between :minj and :maxj "+
				"and pays not like :pays";
		List<Destination> listeMoyennes = session.
				createQuery(moyennesHQL, Destination.class).
				setParameter("minj", minJours).
				setParameter("maxj", maxJours).
				setParameter("pays", eviter).
				list();
		for(Destination d:listeMoyennes)
			System.out.println(" - "+d);

		System.out.println("* Nombre de jours le + élevé : ");
		String maxJoursHQL = "select max(jours) from Destination";
		maxJours = session.createQuery(maxJoursHQL, Integer.class).
				uniqueResult();
		System.out.println("Resultat : "+maxJours+"j");
		
		System.out.println("* Nombre de destinations italiennes : ");
		String nbItalieHQL = "select count(*) from Destination where pays=:pays";
		long nbItalie = session.createQuery(nbItalieHQL, Long.class).
				setParameter("pays", "Italie").
				uniqueResult();
		System.out.println("Resultat : "+nbItalie);		
	}

	private static void lectureParSQL(Session session) {
		System.out.println("*** Lecture par SQL");
		session.clear();
		String toutesSaufPremiereSQL = 
				"select * from destinations where identifiant>:id order by nom";
		List<Destination> liste = session.
				createSQLQuery(toutesSaufPremiereSQL).
				addEntity(Destination.class).
				setParameter("id", 1).list();
		for(Destination d:liste)
			System.out.println(" - "+d);
	}

	private static void insertionsAvecRelations(Session session) {
		System.out.println("*** Insertions avec relations");
		session.clear();
		Transaction tx = session.beginTransaction();
		
		Destination d1 = session.load(Destination.class, 1); // Cordoue
		Destination d4 = session.load(Destination.class, 4); // Venise
		Destination d6 = session.load(Destination.class, 6); // Sintra
		
		Attraction a1 = new Attraction("Mezquita", "Centre", 0);
		a1.setDestination(d1);
		session.save(a1);		
		Attraction a2 = new Attraction("Alcazar", "Centre", 10.90);
		a2.setDestination(d1);
		session.save(a2);		
		Attraction a3 = new Attraction("Basilique", "San Marco", 0);
		a3.setDestination(d4);
		session.save(a3);		
		Attraction a4 = new Attraction("Palais royal", "Centre", 12.0);
		a4.setDestination(d6);
		session.save(a4);
		
		// ajouter en même temps une nouvelle attraction et une nouvelle dest.
		Attraction a5 = new Attraction("Manneken-Pis", "Rue du Chêne", 0);
		Destination d8 = new Destination("Bruxelles", "Belgique", 6);
		a5.setDestination(d8);
		// session.save(a5);
		// session.save(d8);			
		session.persist(a5);
		
		tx.commit();
	}

	private static void lectureAvecRelations(Session session) {
		System.out.println("** Lecture avec relations");
		session.clear();
		
		Attraction a1 = session.load(Attraction.class, 1);
		System.out.println("Pays de la 1ère attraction : "+
				a1.getDestination().getPays());
		
		/// HQL
		System.out.println("Toutes les attractions, ordonnées par pays : ");
		String attractionsParPaysHQL = "from Attraction order by destination.pays";		
		List<Attraction> listeParPays = session.
				createQuery(attractionsParPaysHQL, Attraction.class).list();
		for(Attraction a:listeParPays)
			System.out.println(" - "+a);
		
		System.out.println("Les destinations et le prix de leur "+
				"attraction la plus chère : ");
		String destinationEtPlusChereHQL = "select destination.nom, max(prix) "+
				"from Attraction group by destination.id";
		List<Object[]> listeDestinationEtPlusChere = session.
				createQuery(destinationEtPlusChereHQL, Object[].class).list();
		for(Object[] a:listeDestinationEtPlusChere)
			System.out.println(" - "+a[0]+" : "+a[1]+"e");
		
		System.out.println("Prix total pour un pays :");
		String nomPays = "Portugal";
		String totalPaysHQL = "select sum(prix) " +
				"from Attraction where destination.pays like :p";
		Double total = session.createQuery(totalPaysHQL, Double.class).
			setParameter("p", nomPays).uniqueResult();	
		if(total!=null)
			System.out.println(nomPays+" : "+total+"e au total");
	}

	private static void insertionsPlusieursAPlusieurs(Session session) {
		System.out.println("*** Insertions avec relations");
		session.clear();
		Transaction tx = session.beginTransaction();
		
		// remplir des voyages avec des destinations existantes
		List<Destination> destinations = session.createQuery(
				"from Destination order by id", Destination.class).list();
		Voyage v1 = new Voyage("Espagne magnifique", LocalDate.of(2022, 9, 14));		
		v1.getDestinations().add(destinations.get(0));
		v1.getDestinations().add(destinations.get(1));
		session.save(v1);
		
		Voyage v2 = new Voyage("Italie millenaire", LocalDate.of(2022, 8, 4));		
		v2.getDestinations().add(destinations.get(2));
		v2.getDestinations().add(destinations.get(3));
		session.save(v2);
		
		Voyage v3 = new Voyage("Portugal solaire", LocalDate.of(2022, 11, 2));		
		v3.getDestinations().add(destinations.get(4));
		v3.getDestinations().add(destinations.get(5));
		session.save(v3);
		
		Croisiere c1 = new Croisiere("Merveilles de Méditerannee", 
				LocalDate.of(2022, 12, 2), "Costa Concordia 2");		
		c1.getDestinations().add(destinations.get(2));
		c1.getDestinations().add(destinations.get(5));
		session.save(c1);
		
		session.delete(v2);
		
		tx.commit(); 
	}

	private static void lecturePlusieursAPlusieurs(Session session) {
		System.out.println("** Lecture Plusieurs A Plusieurs : ");
		session.clear();
		
		List<Voyage> voyages = session.createQuery(
				"from Voyage order by depart", Voyage.class).list();
		// afficher chaque voyage et toutes ses étapes
		for(Voyage v : voyages) {
			System.out.println("- "+v);
			for(Destination d : v.getDestinations()) {
				System.out.println("- - "+d);					
			}
		}
		
		String pays = "Portugal";
		// String voyagePaysHQL = "from Voyage where destinations.pays = :p";
		String voyagePaysHQL = "select distinct v "+
				"from Voyage v join v.destinations d "+
				"where d.pays = :p";
		List<Voyage> voyagesPays = 
				session.createQuery(voyagePaysHQL, Voyage.class).
				setParameter("p", pays).list();
		System.out.println("Voyages qui passent par "+pays+" :");
		for(Voyage v:voyagesPays)
			System.out.println("- "+v);
	}
}
