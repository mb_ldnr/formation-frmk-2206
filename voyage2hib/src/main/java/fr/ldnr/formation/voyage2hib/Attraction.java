package fr.ldnr.formation.voyage2hib;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author mic
 *
 */
@Entity
@Table(name="attractions")
public class Attraction {
	
	private int id;
	private String nom;
	private String lieu;
	private double prix;
	private Destination destination;
	
	public Attraction() {
		this(null, null, 0);
	}
	
	public Attraction(String nom, String lieu, double prix) {
		super();
		id = 0;
		this.nom = nom;
		this.lieu = lieu;
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return id+" : "+nom+" ("+lieu+") - "+prix+" €";
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(nullable = false, length = 50)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(nullable = false, length = 50)
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	@Column(nullable = false, scale = 2)
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	//@Column(nullable = false, unique = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	public Destination getDestination() {
		return destination;
	}
	public void setDestination(Destination destination) {
		this.destination = destination;
	}
}
