package fr.ldnr.formation.voyage2hib;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author mic
 *
 */
@Entity
@Table(name = "destinations")
public class Destination implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String nom;
	private String pays;
	private int jours;
	
	public Destination() {
		this(null, null, 0);
	}
	
	public Destination(String nom, String pays, int jours) {
		// ctor de Object - vide : super();
		id = 0;
		this.nom = nom;
		this.pays = pays;
		this.jours = jours;
	}

	@Override
	public String toString() {	
		return id+" : "+nom+" ("+pays+") "+jours+"j";
	}
	
	@Column(name="identifiant")
	@Id // indexé et unique
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(length = 50, nullable = false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(length = 40, nullable = false)
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	@Column(nullable=false)
	public int getJours() {
		return jours;
	}
	public void setJours(int jours) {
		this.jours = jours;
	}
	
	@Transient // pour éviter la création d'une colonne
	public int getSemaines() {
		return jours/7;
	}
	public void setSemaines(int semaines) {
		this.jours = semaines * 7;
	}	
}
