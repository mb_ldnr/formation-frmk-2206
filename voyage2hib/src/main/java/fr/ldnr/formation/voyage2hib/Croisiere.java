package fr.ldnr.formation.voyage2hib;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * @author mic
 *
 */
@Entity
// pas si SINGLE_TABLE @Table(name="croiseres")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Croisiere extends Voyage{
	private String bateau;
	// 2ctor, toString, get, set,

	public Croisiere() {
		super();
		bateau = null;
	}
	public Croisiere(String nom, LocalDate depart, String bateau) {
		super(nom, depart);
		this.bateau = bateau;
	}
		
	@Override
	public String toString() {
		return super.toString()+" ~"+bateau+"~";
	}
	@Column(length = 40, nullable=true)
	public String getBateau() {
		return bateau;
	}
	public void setBateau(String bateau) {
		this.bateau = bateau;
	}
	
}
