package fr.ldnr.formation.voyage2hib;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * @author mic
 *
 */
@Entity
@Table(name="voyages")
public class Voyage {
	private int id;
	private String nom;
	private LocalDate depart;
	private List<Destination> destinations;
	
	public Voyage() {
		this(null, null);
	}
	public Voyage(String nom, LocalDate depart) {
		this.id = 0;
		this.nom = nom;
		this.depart = depart;
		this.destinations = new ArrayList<Destination>();
	}
	
	@Override
	public String toString() {
		return id+" : "+nom+" (depart : "+depart+")";
	}
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(length = 50, nullable = false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public LocalDate getDepart() {
		return depart;
	}
	public void setDepart(LocalDate depart) {
		this.depart = depart;
	}
	@ManyToMany//(cascade = CascadeType.REMOVE)
	public List<Destination> getDestinations() {
		return destinations;
	}
	public void setDestinations(List<Destination> destinations) {
		this.destinations = destinations;
	}
	
	
}
